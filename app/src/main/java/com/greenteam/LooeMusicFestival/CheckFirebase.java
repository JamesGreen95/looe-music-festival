package com.greenteam.LooeMusicFestival;

import android.app.Activity;
import android.os.AsyncTask;

public class CheckFirebase extends AsyncTask<Activity, Void, Boolean> {

    public interface FirebaseLoaded {
        void processFinish(Boolean output);
    }

    private FirebaseLoaded delegate = null;

    public CheckFirebase(FirebaseLoaded delegate){
        this.delegate = delegate;
    }

    @Override
    protected Boolean doInBackground(Activity... activities) {
        LooeMusicFestivalApp app = (LooeMusicFestivalApp) activities[0].getApplication();
        while(!app.ismStagesLoaded()) {
        }
        return true;
    }

    @Override
    protected void onPostExecute(Boolean s) {
        super.onPostExecute(s);
        delegate.processFinish(s);
    }
}

package com.greenteam.LooeMusicFestival;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import static android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM;

public class News extends AppCompatActivity implements DownloadImage.ImageResponse {

    RecyclerView mRecycler;
    ProgressBar mProgressBar;

    int MAX_RESULTS = 3;
    final int INITIAL_RESULTS = 3;

    ArrayList<String> mImageLink = new ArrayList<>();
    ArrayList<String> mTitleTexts = new ArrayList<>();
    ArrayList<String> mNewsText = new ArrayList<>();
    ArrayList<Bitmap> mBitmaps = new ArrayList<>();
    ArrayList<String> mLinks = new ArrayList<>();

    Runnable r;
//    boolean runnableContinue = true;
    LooeMusicFestivalApp mApp;

    StringSplits mStringSplits;
    DownloadImage mImageTask;
    Parcelable mRecyclerViewState;
    LinearLayoutManager mLinearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        getSupportActionBar().setDisplayOptions(DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.abs_layout);
        ((TextView) getSupportActionBar().getCustomView().findViewById(R.id.TITLE_TEXT)).setText(getString(R.string.news));

        mImageTask = new DownloadImage(this);
        mStringSplits = new StringSplits();

        mApp = (LooeMusicFestivalApp) getApplication();

        final Handler handler = new Handler();

        final Thread t = new Thread(new Runnable() {
            public void run() {
                while(!Thread.currentThread().isInterrupted()) {
                    while (mApp.getmNewsHTML().isEmpty()) {
                    }
                    stringLoaded();
                    handler.post(this);
                    Thread.currentThread().interrupt();
                }
            }
        });
        t.start();

    }

    private void stringLoaded() {
        sortStrings(mApp.getmNewsHTML());
        decodeImages();
    }

    private void imagesLoaded() {
        mRecycler = (RecyclerView) findViewById(R.id.recycler);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mLinearLayoutManager = new LinearLayoutManager(this);
        NewsListAdapter mNewsListAdapter = new NewsListAdapter(this);
        mRecycler.setAdapter(mNewsListAdapter);
        mRecycler.setLayoutManager(mLinearLayoutManager);
        if (mRecyclerViewState != null) {
            mRecycler.getLayoutManager().onRestoreInstanceState(mRecyclerViewState);
        }
        mProgressBar.setVisibility(View.GONE);
    }

    private void sortStrings(String html) {
        mTitleTexts = mStringSplits.getH2Title(html);
        mLinks = mStringSplits.getLink(html);
        for (String s : mStringSplits.getPText(html)) {
            s = s.substring(0, s.length() - 10);
            s += "[...]";
            mNewsText.add(s);
        }
        mImageLink = mStringSplits.getImageSource(html);
    }

    public void moreButton(View view) {
        mProgressBar.setVisibility(View.VISIBLE);
        MAX_RESULTS += 3;
        if(MAX_RESULTS > mImageLink.size()) {
            MAX_RESULTS = mImageLink.size();
        }
        decodeImages();
        mRecyclerViewState = mRecycler.getLayoutManager().onSaveInstanceState();
    }

    public void back_button(View view) {
        Intent intent = new Intent(News.this, MainActivity.class);
        startActivity(intent);
    }

    private class NewsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private LayoutInflater mLayoutInflater;

        private NewsListAdapter(Context context) {
            mLayoutInflater = LayoutInflater.from(context);
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == 0) {
                return new NewsViewHolder(mLayoutInflater
                        .inflate(R.layout.recycler_news, parent, false));
            } else {
                return new NewsViewHolderBreaker(mLayoutInflater
                        .inflate(R.layout.recycler_break, parent, false));
            }
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if (holder.getItemViewType() == 0) {
                NewsViewHolder itemHolder = (NewsViewHolder) holder;
                String title = mStringSplits.removeDefects(getTitle(position));
                String text = mStringSplits.removeDefects(getNewsText(position));
                itemHolder.setData(title, text, getImage(position), getLink(position));
            }
        }

        @Override
        public int getItemViewType(int position) {
            if (MAX_RESULTS > position) {
                return 0;
            } else if(mLinks.size() != MAX_RESULTS){
                return 1;
            }
            return 0;
        }

        @Override
        public int getItemCount() {
            return MAX_RESULTS + 1;
        }
    }

    private class NewsViewHolder extends RecyclerView.ViewHolder {

        ImageView mPicture;
        TextView mTitle;
        TextView mNews;
        TextView mMore;

        private NewsViewHolder(View itemView) {
            super(itemView);

            mPicture = (ImageView) itemView.findViewById(R.id.image_news);
            mTitle = (TextView) itemView.findViewById(R.id.title_text);
            mNews = (TextView) itemView.findViewById(R.id.news_text);
            mMore = (TextView) itemView.findViewById(R.id.more_text);
        }

        private void setData(String title, String text, Bitmap image, final String link) {
            View.OnClickListener onClick = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    moreNewsPage(link);
                }
            };
            mPicture.setImageBitmap(image);
            mPicture.setOnClickListener(onClick);
            mTitle.setText(title);
            mTitle.setOnClickListener(onClick);
            mNews.setText(text);
            mMore.setOnClickListener(onClick);
        }
    }

    private class NewsViewHolderBreaker extends RecyclerView.ViewHolder {
        private NewsViewHolderBreaker(View itemView) {
            super(itemView);
        }

    }

    public Bitmap getImage(int position) {
        return mBitmaps.get(position);
    }

    public void decodeImages() {
        mImageTask = new DownloadImage(this);
        String[] strings = new String[INITIAL_RESULTS];
        for (int i = 0; i < INITIAL_RESULTS; i++) {
            strings[i] = mImageLink.get((MAX_RESULTS - INITIAL_RESULTS) + i);
        }
        mImageTask.execute(strings);
    }

    public String getNewsText(int position) {
        return mNewsText.get(position);
    }

    public String getTitle(int position) {
        return mTitleTexts.get(position);
    }

    public String getLink(int position) {
        return mLinks.get(position);
    }

    @Override
    public void ImageFinished(ArrayList<Bitmap> bitmaps) {
        for (Bitmap b : bitmaps) {
            mBitmaps.add(b);
        }
        imagesLoaded();
    }

    private void moreNewsPage(String URL) {
        Intent intent = new Intent(this, NewsOpenActivity.class);
        intent.putExtra("URL", URL);
        startActivity(intent);
    }
}

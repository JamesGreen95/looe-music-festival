package com.greenteam.LooeMusicFestival;

import java.util.ArrayList;

public class Stage {
    private ArrayList<String> times = new ArrayList<>();
    private ArrayList<String> bands = new ArrayList<>();
    private String stageName;

    public Stage(String name) {
        stageName = name;
    }

    public void addBand(String time, String band) {
        times.add(time);
        bands.add(band);
    }

    public String getStageName() {
        return stageName;
    }

    public ArrayList<String> getBands() {
        return bands;
    }

    public ArrayList<String> getTimes() {
        return times;
    }
}

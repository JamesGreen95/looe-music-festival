package com.greenteam.LooeMusicFestival;

import android.app.ListFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.twitter.sdk.android.tweetui.SearchTimeline;
import com.twitter.sdk.android.tweetui.TweetTimelineListAdapter;
import com.twitter.sdk.android.tweetui.UserTimeline;

public class TwitterFeed extends ListFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final UserTimeline userTimeline = new UserTimeline.Builder()
                .screenName("looemusicfest")
                .maxItemsPerRequest(10)
                .build();

//        final SearchTimeline searchTimeline = new SearchTimeline.Builder()
//                .query("from:looemusicfest")
//                .maxItemsPerRequest(10)
//                .build();

        final TweetTimelineListAdapter adapter = new TweetTimelineListAdapter.Builder(getActivity())
                .setTimeline(userTimeline)
                .build();

        setListAdapter(adapter);
    }
}

package com.greenteam.LooeMusicFestival;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.TextView;

public class h2TextView extends TextView {
    public h2TextView(Context context) {
        super(context);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            this.setTextColor(getResources().getColor(R.color.colorPrimary, null));
        } else {
            this.setTextColor(getResources().getColor(R.color.colorPrimary));
        }
        this.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        this.setGravity(Gravity.CENTER);
    }

    public h2TextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            this.setTextColor(getResources().getColor(R.color.colorPrimary, null));
        } else {
            this.setTextColor(getResources().getColor(R.color.colorPrimary));
        }
        this.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        this.setGravity(Gravity.CENTER);
    }
}

package com.greenteam.LooeMusicFestival;

import java.util.ArrayList;

public class Day {
    private ArrayList<Stage> stages = new ArrayList<>();
    private String day;

    public Day(String dayString) {
        day = dayString;
    }

    public void addStage(Stage stage) {
        stages.add(stage);
    }

    public ArrayList<Stage> getStages() {
        return stages;
    }

    public String getDay() {
        return day;
    }
}

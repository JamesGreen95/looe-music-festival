package com.greenteam.LooeMusicFestival;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.TextView;

public class pTextView extends android.support.v7.widget.AppCompatTextView {
    public pTextView(Context context) {
        super(context);
        this.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
    }

    public pTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
    }
}

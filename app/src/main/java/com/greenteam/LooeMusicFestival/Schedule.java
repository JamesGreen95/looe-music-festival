package com.greenteam.LooeMusicFestival;

import android.content.Intent;
import android.os.Build;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import static android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM;

public class Schedule extends AppCompatActivity implements CheckFirebase.FirebaseLoaded {

    private int numOfPages;
    private ViewPager mViewPager;
    private TextView mTopText;
    private ArrayList<String> mStageNames;
    private ArrayList<ImageView> mButtons = new ArrayList<>();

    private ArrayList<ScheduleViewPager> mPages = new ArrayList<>();

    private String mCurrentDay = "friday";
    private ArrayList<Day> mDays;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);

        CheckFirebase checkFirebase = new CheckFirebase(this);
        checkFirebase.execute(this);

    }

    private void finishedLoading() {

        ProgressBar progressLayout = (ProgressBar) findViewById(R.id.progress_bar);
        LinearLayout mainLayout = (LinearLayout) findViewById(R.id.main_page_layout);
        LinearLayout dayLayout = (LinearLayout) findViewById(R.id.day_layout);

        mStageNames = new ArrayList<>();

        ImageView fridayButton = (ImageView) findViewById(R.id.friday_button);
        ImageView saturdayButton = (ImageView) findViewById(R.id.saturday_button);
        ImageView sundayButton = (ImageView) findViewById(R.id.sunday_button);

        mButtons.add(fridayButton);
        mButtons.add(saturdayButton);
        mButtons.add(sundayButton);

        LooeMusicFestivalApp app = (LooeMusicFestivalApp) getApplication();

        for (Stage s : app.getmDays().get(0).getStages()) {
            mStageNames.add(s.getStageName());
        }
        mDays = app.getmDays();
        numOfPages = mStageNames.size();

        getSupportActionBar().setDisplayOptions(DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.abs_layout);
        ((TextView) getSupportActionBar().getCustomView().findViewById(R.id.TITLE_TEXT)).setText(getString(R.string.schedule));

        mViewPager = (ViewPager) findViewById(R.id.schedule_view_pager);
        PagerAdapter mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mPagerAdapter);

        mViewPager.setOffscreenPageLimit(numOfPages);

        mTopText = (TextView) findViewById(R.id.schedule_top_text);
        mTopText.setText(mStageNames.get(0));

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mTopText.setText(mStageNames.get(position));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        progressLayout.setVisibility(View.GONE);
        mainLayout.setVisibility(View.VISIBLE);
        dayLayout.setVisibility(View.VISIBLE);

    }

    public void left_button(View view) {
        if (mViewPager.getCurrentItem() != 0) {
            mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1, true);
        }
    }

    public void right_button(View view) {
        if (mViewPager.getCurrentItem() != numOfPages - 1) {
            mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1, true);
        }
    }

    public void back_button(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void processFinish(Boolean output) {
        finishedLoading();
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            ScheduleViewPager newPage = new ScheduleViewPager();
            Bundle currentStageInfo = new Bundle();
            currentStageInfo.putString("Stage_Name", mStageNames.get(position));
            newPage.setArguments(currentStageInfo);
            mPages.add(newPage);
            return newPage;
        }

        @Override
        public int getCount() {
            return numOfPages;
        }
    }

    public void dayButtons(View view) {
        ImageView button = (ImageView) view;
        String oldDay = mCurrentDay;
        switch (button.getTag().toString()) {
            case "friday":
                if (!mCurrentDay.equals("friday")) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        button.setImageDrawable(getDrawable(R.drawable.friday_button_down));
                        for (int i = 0; i < mButtons.size(); i++) {
                            switch (mButtons.get(i).getTag().toString()) {
                                case "saturday":
                                    mButtons.get(i).setImageDrawable(getDrawable(R.drawable.saturday_button));
                                    break;
                                case "sunday":
                                    mButtons.get(i).setImageDrawable(getDrawable(R.drawable.sunday_button));
                                    break;
                                default:
                                    break;
                            }
                        }
                    } else {
                        button.setImageDrawable(getResources().getDrawable(R.drawable.friday_button_down));
                        for (int i = 0; i < mButtons.size(); i++) {
                            switch (mButtons.get(i).getTag().toString()) {
                                case "saturday":
                                    mButtons.get(i).setImageDrawable(getResources().getDrawable(R.drawable.saturday_button));
                                    break;
                                case "sunday":
                                    mButtons.get(i).setImageDrawable(getResources().getDrawable(R.drawable.sunday_button));
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    mCurrentDay = "friday";
                }
                break;
            case "saturday":
                if (!mCurrentDay.equals("saturday")) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        button.setImageDrawable(getDrawable(R.drawable.saturday_button_down));
                        for (int i = 0; i < mButtons.size(); i++) {
                            switch (mButtons.get(i).getTag().toString()) {
                                case "friday":
                                    mButtons.get(i).setImageDrawable(getDrawable(R.drawable.friday_button));
                                    break;
                                case "sunday":
                                    mButtons.get(i).setImageDrawable(getDrawable(R.drawable.sunday_button));
                                    break;
                                default:
                                    break;
                            }
                        }
                    } else {
                        button.setImageDrawable(getResources().getDrawable(R.drawable.saturday_button_down));
                        for (int i = 0; i < mButtons.size(); i++) {
                            switch (mButtons.get(i).getTag().toString()) {
                                case "friday":
                                    mButtons.get(i).setImageDrawable(getResources().getDrawable(R.drawable.friday_button));
                                    break;
                                case "sunday":
                                    mButtons.get(i).setImageDrawable(getResources().getDrawable(R.drawable.sunday_button));
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    mCurrentDay = "saturday";
                }
                break;
            case "sunday":
                if (!mCurrentDay.equals("sunday")) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        button.setImageDrawable(getDrawable(R.drawable.sunday_button_down));
                        for (int i = 0; i < mButtons.size(); i++) {
                            switch (mButtons.get(i).getTag().toString()) {
                                case "saturday":
                                    mButtons.get(i).setImageDrawable(getDrawable(R.drawable.saturday_button));
                                    break;
                                case "friday":
                                    mButtons.get(i).setImageDrawable(getDrawable(R.drawable.friday_button));
                                    break;
                                default:
                                    break;
                            }
                        }
                    } else {
                        button.setImageDrawable(getResources().getDrawable(R.drawable.sunday_button_down));
                        for (int i = 0; i < mButtons.size(); i++) {
                            switch (mButtons.get(i).getTag().toString()) {
                                case "saturday":
                                    mButtons.get(i).setImageDrawable(getResources().getDrawable(R.drawable.saturday_button));
                                    break;
                                case "friday":
                                    mButtons.get(i).setImageDrawable(getResources().getDrawable(R.drawable.friday_button));
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    mCurrentDay = "sunday";
                }
                break;
        }
        for (int i = 0; i < mPages.size(); i++) {
            int count = 0;
            for (Day d : mDays) {
                if (d.getDay().equalsIgnoreCase(oldDay)) {
                    ArrayList<Parcelable> bands = mPages.get(count).getmBandStates();
                    ArrayList<Parcelable> times = mPages.get(count).getmTimeStates();
                    for (ScheduleViewPager p : mPages) {
                        if(mPages.get(i) != p) {
                            p.setmBandStates(bands);
                            p.setmTimeStates(times);
                        }
                    }
                }
                count++;
            }
            mPages.get(i).refreshSchedule(mCurrentDay);
        }
    }
}

package com.greenteam.LooeMusicFestival;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;

public class DownloadPage extends AsyncTask<String, Void, String> {

    public interface PageResponse {
        void processFinish(String output);
    }

    private PageResponse delegate = null;

    public DownloadPage(PageResponse delegate) {
        this.delegate = delegate;
    }

    @Override
    protected String doInBackground(String... urls) {
        URLConnection connection;
        try {
            URL url = new URL(urls[0]);

            connection = url.openConnection();

            String html;
            InputStream inputStream = connection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder str = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                str.append(line);
            }
            inputStream.close();
            html = str.toString();

            return html;

        } catch (MalformedURLException e) {
            e.printStackTrace();
            return "Failed";
        } catch (IOException e) {
            e.printStackTrace();
            return "Failed";
        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        delegate.processFinish(s);
    }
}

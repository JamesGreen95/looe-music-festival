package com.greenteam.LooeMusicFestival;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class DownloadImage extends AsyncTask<String[], Void, ArrayList<Bitmap>> {

    public interface ImageResponse {
        void ImageFinished(ArrayList<Bitmap> bitmaps);
    }

    public ImageResponse delegate = null;

    public DownloadImage(ImageResponse delegate) {
        this.delegate = delegate;
    }

    @Override
    protected ArrayList<Bitmap> doInBackground(String[]... strings) {
        ArrayList<Bitmap> bitmaps = new ArrayList<>();
        String[] urls = strings[0];
        try {
            for (int i = 0; i < urls.length; i++) {

                URL url = new URL(urls[i]);

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                connection.connect();

                InputStream inputStream = connection.getInputStream();

                bitmaps.add(BitmapFactory.decodeStream(inputStream));
            }
            return bitmaps;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(ArrayList<Bitmap> bitmaps) {
        super.onPostExecute(bitmaps);
        delegate.ImageFinished(bitmaps);
    }
}

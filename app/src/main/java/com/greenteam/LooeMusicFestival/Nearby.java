package com.greenteam.LooeMusicFestival;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import static android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM;

public class Nearby extends AppCompatActivity {


    private static final int NUM_OF_PAGES = 3;
    private ViewPager mViewPager;
    PagerAdapter mPagerAdapter;
    private TextView mTopText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearby);

        final String[] NEARBY_TYPES = {this.getString(R.string.things_to_do),this.getString(R.string.accommodation),this.getString(R.string.eat_drink)};

        getSupportActionBar().setDisplayOptions(DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.abs_layout);
        ((TextView)getSupportActionBar().getCustomView().findViewById(R.id.TITLE_TEXT)).setText(getString(R.string.nearby));

        mViewPager = (ViewPager) findViewById(R.id.nearby_view_pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.setOffscreenPageLimit(3);
        mTopText = (TextView) findViewById(R.id.nearby_top_text);
        mTopText.setText(NEARBY_TYPES[0]);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mTopText.setText(NEARBY_TYPES[position]);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void left_button(View view) {
        if(mViewPager.getCurrentItem() != 0) {
            mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1, true);
        }
    }

    public void right_button(View view) {
        if(mViewPager.getCurrentItem() != NUM_OF_PAGES-1) {
            mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1, true);
        }
    }

    public void back_button(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch(position) {
                case 0:
                    return new ThingsToDoFragment();
                case 1:
                    return new AccommodationFragment();
                case 2:
                    return new EatAndDrinkFragment();
            }
            return null;
        }

        @Override
        public int getCount() {
            return NUM_OF_PAGES;
        }
    }

}


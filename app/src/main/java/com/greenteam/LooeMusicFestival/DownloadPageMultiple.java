package com.greenteam.LooeMusicFestival;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class DownloadPageMultiple extends AsyncTask<String, Void, ArrayList<String[]>> {

    public interface PageResponse {
        void processFinish(ArrayList<String[]> output);
    }

    private PageResponse delegate = null;

    public DownloadPageMultiple(PageResponse delegate) {
        this.delegate = delegate;
    }

    @Override
    protected ArrayList<String[]> doInBackground(String... urls) {
        URLConnection connection;
        ArrayList<String[]> arrays = new ArrayList<>();
        try {
            for (String s : urls) {
                URL url = new URL(s);

                String[] array = new String[2];
                connection = url.openConnection();

                String html;
                InputStream inputStream = connection.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder str = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    str.append(line);
                }
                inputStream.close();
                html = str.toString();
                array[0] = s;
                array[1] = html;
                arrays.add(array);
            }
            return arrays;

        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(ArrayList<String[]> s) {
        super.onPostExecute(s);
        delegate.processFinish(s);
    }
}

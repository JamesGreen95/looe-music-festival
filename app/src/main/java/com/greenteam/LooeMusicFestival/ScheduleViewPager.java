package com.greenteam.LooeMusicFestival;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class ScheduleViewPager extends Fragment {

    Context mContext;
    CustomRecyclerview mTimeRecycler;
    CustomRecyclerview mBandRecycler;
    Stage mCurrentStage;
    String mStageName;
    BandListAdapter mBandListAdapter;
    ArrayList<Day> mDays = new ArrayList<>();

    ArrayList<String> mUniqueBands = new ArrayList<>();
    ArrayList<Integer> mBandSizes = new ArrayList<>();

    Day mCurrentDay;
    String mDay = "friday";

    ArrayList<Parcelable> mTimeStates = new ArrayList<>();
    ArrayList<Parcelable> mBandStates = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_schedule, container, false);
        mContext = rootView.getContext();
        mTimeRecycler = (CustomRecyclerview) rootView.findViewById(R.id.time_recycler);
        mBandRecycler = (CustomRecyclerview) rootView.findViewById(R.id.bands_recycler);

        LooeMusicFestivalApp app = (LooeMusicFestivalApp) getActivity().getApplication();

        mStageName = getArguments().getString("Stage_Name");

        mDays = app.getmDays();

        defineBands();

        startActivity();

        return rootView;
    }

    private void startActivity() {

        LinearLayoutManager timeLayoutManager = new LinearLayoutManager(mContext);
        TimeListAdapter timeListAdapater = new TimeListAdapter(mContext);
        mTimeRecycler.setAdapter(timeListAdapater);
        mTimeRecycler.setLayoutManager(timeLayoutManager);

        LinearLayoutManager bandLayoutManager = new LinearLayoutManager(mContext);
        mBandListAdapter = new BandListAdapter(mContext);
        mBandRecycler.setAdapter(mBandListAdapter);
        mBandRecycler.setLayoutManager(bandLayoutManager);

        RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (recyclerView == mTimeRecycler) {
                    mBandRecycler.removeOnScrollListener(this);
                    mBandRecycler.scrollBy(0, dy);
                    mBandRecycler.addOnScrollListener(this);
                } else if (recyclerView == mBandRecycler) {
                    mTimeRecycler.removeOnScrollListener(this);
                    mTimeRecycler.scrollBy(0, dy);
                    mTimeRecycler.addOnScrollListener(this);
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        };

        mTimeRecycler.addOnScrollListener(scrollListener);
        mBandRecycler.addOnScrollListener(scrollListener);

        mBandRecycler.getRecycledViewPool().setMaxRecycledViews(0, 0);

        for(int i = 0;i<mDays.size();i++) {
            mBandStates.add(null);
            mTimeStates.add(null);
        }
    }

    private class TimeListAdapter extends RecyclerView.Adapter<TimeViewHolder> {

        private LayoutInflater mLayoutInflater;

        public TimeListAdapter(Context context) {
            mLayoutInflater = LayoutInflater.from(context);
        }

        @Override
        public TimeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new TimeViewHolder(mLayoutInflater
                    .inflate(R.layout.recycler_time, parent, false));
        }

        @Override
        public void onBindViewHolder(TimeViewHolder holder, int position) {
            holder.setData(mCurrentStage.getTimes().get(position));
        }

        @Override
        public int getItemCount() {
            return mCurrentStage.getTimes().size();
        }
    }

    private class TimeViewHolder extends RecyclerView.ViewHolder {


        TextView mTime;

        public TimeViewHolder(View itemView) {
            super(itemView);

            mTime = (TextView) itemView.findViewById(R.id.time_text);

        }

        public void setData(String time) {
            mTime.setText(time);
        }
    }


    private class BandListAdapter extends RecyclerView.Adapter<BandViewHolder> {

        private LayoutInflater mLayoutInflater;

        public BandListAdapter(Context context) {
            mLayoutInflater = LayoutInflater.from(context);
        }

        @Override
        public BandViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new BandViewHolder(mLayoutInflater
                    .inflate(R.layout.recycler_band, parent, false));
        }

        @Override
        public void onBindViewHolder(BandViewHolder holder, int position) {
            holder.setData(mUniqueBands.get(position), position, mBandSizes.get(position));
        }

        @Override
        public int getItemCount() {
            return mUniqueBands.size();
        }
    }

    private class BandViewHolder extends RecyclerView.ViewHolder {

        TextView mBandText;
        RelativeLayout mLayout;

        public BandViewHolder(View itemView) {
            super(itemView);
            mBandText = (TextView) itemView.findViewById(R.id.band_text);
            mLayout = (RelativeLayout) itemView;
        }

        public void setData(String band, int position, int number) {
            int height = getDp(number * 60);
            RelativeLayout.LayoutParams textParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height);
            mBandText.setLayoutParams(textParams);
            if (band.isEmpty()) {
                mBandText.setVisibility(View.INVISIBLE);
            }

            if (position == 0) {
                RecyclerView.LayoutParams params = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.setMargins(0, getDp(14), 0, 0);
                mLayout.setLayoutParams(params);
            }
            mBandText.setText(band);
        }
    }

    private int getDp(int value) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, getResources().getDisplayMetrics());
    }


    private void defineBands() {
        for (Day d : mDays) {
            if (d.getDay().equalsIgnoreCase(mDay)) {
                mCurrentDay = d;
                for (Stage s : mCurrentDay.getStages()) {
                    if (s.getStageName().equals(mStageName)) {
                        mCurrentStage = s;

                        String currentBand = "";
                        int number = 0;

                        boolean first = true;
                        for (String string : mCurrentStage.getBands()) {
                            if (first && string.isEmpty()) {
                                mUniqueBands.add(string);
                                number++;
                                first = false;
                            } else if (!string.equalsIgnoreCase(currentBand)) {
                                mUniqueBands.add(string);
                                currentBand = string;
                                if (!first) {
                                    mBandSizes.add(number);
                                }
                                number = 1;
                                first = false;
                            } else {
                                number++;
                            }
                        }
                        mBandSizes.add(number);
                    }
                }
            }
        }
    }

    public void refreshSchedule(String s) {
        preserveScrolls(s);
        mUniqueBands.clear();
        mBandSizes.clear();
        defineBands();
        mBandListAdapter.notifyDataSetChanged();
    }

    public void preserveScrolls(String s) {
        for(int i = 0;i<mDays.size();i++) {
            if(mDays.get(i).getDay().equalsIgnoreCase(mDay)) {
                mTimeStates.set(i, mTimeRecycler.onSaveInstanceState());
                mBandStates.set(i, mBandRecycler.onSaveInstanceState());
            }
        }
        mDay = s;
        for(int o = 0;o<mDays.size();o++) {
            if(mDays.get(o).getDay().equalsIgnoreCase(mDay)) {
                if(mTimeStates.get(o) != null) {
                    mTimeRecycler.onRestoreInstanceState(mTimeStates.get(o));
                } else {
                    mTimeRecycler.scrollToPosition(0);
                }
                if(mBandStates.get(o) != null) {
                    mBandRecycler.onRestoreInstanceState(mBandStates.get(o));
                } else {
                    mBandRecycler.scrollToPosition(0);
                }
            }
        }

    }

    public ArrayList<Parcelable> getmBandStates() {
        return mBandStates;
    }

    public ArrayList<Parcelable> getmTimeStates() {
        return mTimeStates;
    }

    public void setmTimeStates(ArrayList<Parcelable> mTimeStates) {
        this.mTimeStates = mTimeStates;
    }

    public void setmBandStates(ArrayList<Parcelable> mBandStates) {
        this.mBandStates = mBandStates;
    }
}

package com.greenteam.LooeMusicFestival;


import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import static android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM;

public class NewsOpenActivity extends AppCompatActivity implements DownloadImage.ImageResponse, DownloadPage.PageResponse {


    TextView mTitle;
    LinearLayout mLayout;
    ProgressBar mProgressBar;

    ArrayList<ImageView> mImageViews = new ArrayList<>();
    ArrayList<Boolean> mImagesLoaded = new ArrayList<>();

    int mImageCount = 0;


    DownloadImage mDownloadImageTask;
    DownloadPage mDownloadPageTask;
    StringSplits mStringSplits;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_open);

        mTitle = (TextView) findViewById(R.id.title_text);
        mLayout = (LinearLayout) findViewById(R.id.news_layout);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);

        getSupportActionBar().setDisplayOptions(DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.abs_layout);
        ((TextView) getSupportActionBar().getCustomView().findViewById(R.id.TITLE_TEXT)).setText(getString(R.string.news));

        mDownloadPageTask = new DownloadPage(this);
        mStringSplits = new StringSplits();

        String url = getIntent().getStringExtra("URL");

        mDownloadPageTask.execute(url);
    }

    @Override
    public void ImageFinished(ArrayList<Bitmap> bitmaps) {
        for (int i = 0; i < mImageViews.size(); i++) {
            if (!mImagesLoaded.get(i)) {
                mImageViews.get(i).setImageBitmap(bitmaps.get(0));
                mImagesLoaded.set(i,true);
                i = mImageViews.size();
            }
        }
        if(mImagesLoaded.get(mImagesLoaded.size()-1)) {
            mLayout.setVisibility(View.VISIBLE);
            mProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void processFinish(String output) {
        output = mStringSplits.getFullNewsPageSplit(output);
        determineOrder(output);
    }

    public void back_button(View view) {
        finish();
    }

    private void determineOrder(String output) {
        ArrayList<String> strings = mStringSplits.analysePage(output);
        for (String s : strings) {
            if (s.contains("src")) {
                mImageCount++;
            }
        }
        for (int i = 0; i < strings.size(); i++) {
            String s = strings.get(i);
            if (s.contains("src")) {
                ImageView image = new ImageView(this);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, getDp(220));
                params.setMargins(0,getDp(10),0,getDp(20));
                image.setLayoutParams(params);
                image.setScaleType(ImageView.ScaleType.FIT_CENTER);
                mLayout.addView(image);
                mImageViews.add(image);
                mImagesLoaded.add(false);
                mDownloadImageTask = new DownloadImage(this);
                String[] url = {mStringSplits.getImageSource(s).get(0)};
                mDownloadImageTask.execute(url);
            } else if (s.contains("<p>")) {
                pTextView text = new pTextView(this);
                mLayout.addView(text);
                text.setGravity(Gravity.CENTER);
                text.setText(mStringSplits.removeDefects(mStringSplits.getPText(s).get(0)) + "\n");
            } else if (s.contains("h2")) {
                h2TextView text = new h2TextView(this);
                mLayout.addView(text);
                text.setText(mStringSplits.removeDefects(mStringSplits.getH2Title(s).get(0)) + "\n");
            } else if (s.contains("h1")) {
                mTitle.setText(mStringSplits.getH1Title(s).get(0));
            }
        }
    }

    private int getDp(int value) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, getResources().getDisplayMetrics());
    }
}

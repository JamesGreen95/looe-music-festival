package com.greenteam.LooeMusicFestival;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

public class EatAndDrinkFragment extends Fragment implements DownloadImage.ImageResponse{

    ViewGroup mRootView;
    RecyclerView mRecycler;
    ProgressBar mProgressBar;

    int MAX_RESULTS = 3;
    int INITIAL_RESULTS = 3;

    DownloadImage mDownloadImage;
    StringSplits mStringSplits;

    ArrayList<String> mImageLink = new ArrayList<>();
    ArrayList<String> mTitleTexts = new ArrayList<>();
    ArrayList<String> mDescriptions = new ArrayList<>();
    ArrayList<Bitmap> mBitmaps = new ArrayList<>();
    ArrayList<String> mLinks = new ArrayList<>();

    LooeMusicFestivalApp mApp;

    Parcelable mRecyclerViewState;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = (ViewGroup) inflater.inflate(R.layout.fragment_nearby, container, false);
        mProgressBar = (ProgressBar) mRootView.findViewById(R.id.progress_bar);

        mStringSplits = new StringSplits();

        mApp = (LooeMusicFestivalApp) getActivity().getApplication();

        final Handler handler = new Handler();

        final Thread t = new Thread(new Runnable() {
            public void run() {
                while(!Thread.currentThread().isInterrupted()) {
                    while (mApp.getmEatHTML().isEmpty()) {
                    }
                    HTMLLoaded();
                    handler.post(this);
                    Thread.currentThread().interrupt();
                }
            }
        });
        t.start();

        return mRootView;
    }

    private void HTMLLoaded() {
        sortStrings(mApp.getmEatHTML());
        decodeImages();
    }

    private void sortStrings(String html) {
        mTitleTexts = mStringSplits.getH2Title(html);
        mLinks = mStringSplits.getLink(html);
        mDescriptions = mStringSplits.getPText(html);
        mImageLink = mStringSplits.getBackgroundSource(html);
    }

    public void decodeImages() {
        mDownloadImage = new DownloadImage(this);
        String[] strings = new String[INITIAL_RESULTS];
        for (int i = 0; i < INITIAL_RESULTS; i++) {
            strings[i] = mImageLink.get((MAX_RESULTS - INITIAL_RESULTS) + i);
        }
        mDownloadImage.execute(strings);
    }

    private void imagesLoaded() {
        mRecycler = (RecyclerView) mRootView.findViewById(R.id.recycler);
        mProgressBar = (ProgressBar) mRootView.findViewById(R.id.progress_bar);
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(mRootView.getContext());
        NearbyListAdapter adapter = new NearbyListAdapter(mRootView.getContext());
        mRecycler.setAdapter(adapter);
        mRecycler.setLayoutManager(mLinearLayoutManager);
        if (mRecyclerViewState != null) {
            mRecycler.getLayoutManager().onRestoreInstanceState(mRecyclerViewState);
        }
        mProgressBar.setVisibility(View.GONE);
    }

    public void moreButton() {
        mProgressBar.setVisibility(View.VISIBLE);
        MAX_RESULTS += 3;
        if(MAX_RESULTS > mImageLink.size()) {
            MAX_RESULTS = mImageLink.size()-1;
        }
        decodeImages();
        mRecyclerViewState = mRecycler.getLayoutManager().onSaveInstanceState();
    }

    private class NearbyListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private LayoutInflater mLayoutInflater;

        public NearbyListAdapter(Context context) {
            mLayoutInflater = LayoutInflater.from(context);
        }

        @Override
        public int getItemViewType(int position) {
            if(position< MAX_RESULTS) {
                return 0;
            } else if(mLinks.size()-1 != MAX_RESULTS){
                return 1;
            }
            return 0;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == 0) {
                return new AccomViewHolder(mLayoutInflater
                        .inflate(R.layout.recycler_nearby, parent, false));
            } else {
                return new Breaker(mLayoutInflater
                        .inflate(R.layout.recycler_break, parent, false));
            }
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if(holder.getItemViewType() == 0) {
                AccomViewHolder holders = (AccomViewHolder) holder;
                String title = mStringSplits.removeDefects(mTitleTexts.get(position).substring(0,1).toUpperCase() + mTitleTexts.get(position).substring(1));
                String text = mStringSplits.removeDefects(mDescriptions.get(position));
                holders.setData(title,text, mBitmaps.get(position), mLinks.get(position));
            }
        }

        @Override
        public int getItemCount() {
            return MAX_RESULTS+1;
        }
    }

    private class AccomViewHolder extends RecyclerView.ViewHolder {

        ImageView mPicture;
        TextView mTitle;
        TextView mText;
        TextView mMore;

        public AccomViewHolder(View itemView) {
            super(itemView);

            mPicture = (ImageView) itemView.findViewById(R.id.picture_layout);
            mTitle = (TextView) itemView.findViewById(R.id.title_text);
            mText = (TextView) itemView.findViewById(R.id.description_text);
            mMore = (TextView) itemView.findViewById(R.id.more_text);
        }

        public void setData(String title, String text, Bitmap bitmap, final String link) {
            View.OnClickListener onClick = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openItem(link);
                }
            };
            mPicture.setImageBitmap(bitmap);
            mPicture.setOnClickListener(onClick);
            mTitle.setText(title);
            mTitle.setOnClickListener(onClick);
            mText.setText(text);
            mMore.setOnClickListener(onClick);
        }
    }

    private class Breaker extends RecyclerView.ViewHolder {
        public Breaker(View itemView) {
            super(itemView);
            Button more = (Button) itemView.findViewById(R.id.more_button);
            more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    moreButton();
                }
            });
        }
    }


    @Override
    public void ImageFinished(ArrayList<Bitmap> bitmaps) {
        for (Bitmap b : bitmaps) {
            mBitmaps.add(b);
        }
        imagesLoaded();
    }

    private void openItem(String URL) {
        Intent intent = new Intent(getActivity(), NearbyOpenActivity.class);
        intent.putExtra("URL", URL);
        startActivity(intent);
    }
}

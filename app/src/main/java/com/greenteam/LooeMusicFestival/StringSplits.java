package com.greenteam.LooeMusicFestival;

import android.os.Build;
import android.text.Html;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringSplits {

    public ArrayList<String> getImageSource(String html) {
        ArrayList<String> strings = new ArrayList<>();
        Pattern image = Pattern.compile("src=\"(.*?)\"");
        Matcher m = image.matcher(html);
        while (m.find()) {
            strings.add(m.group(1));
        }
        return strings;
    }

    public ArrayList<String> getBackgroundSource(String html) {
        ArrayList<String> strings = new ArrayList<>();
        Pattern image = Pattern.compile("style=\"background-image:url(.*?);");
        Matcher m = image.matcher(html);
        while (m.find()) {
            strings.add(m.group(1).substring(1, m.group(1).length()-1));
        }
        return strings;
    }

    public ArrayList<String> getH2Title(String html) {
        ArrayList<String> strings = new ArrayList<>();
        Pattern title = Pattern.compile("<h2>(.*?)</h2>");
        Matcher m = title.matcher(html);
        while (m.find()) {
            strings.add(m.group(1));
        }
        return strings;
    }

    public ArrayList<String> getH1Title(String html) {
        ArrayList<String> strings = new ArrayList<>();
        Pattern title = Pattern.compile("<h1>(.*?)</h1>");
        Matcher m = title.matcher(html);
        while (m.find()) {
            strings.add(m.group(1));
        }
        return strings;
    }
    public ArrayList<String> getPText(String html) {
        ArrayList<String> strings = new ArrayList<>();
        Pattern paragraph = Pattern.compile("<p.*?>(.*?)</p>");
        Matcher m = paragraph.matcher(html);
        while (m.find()) {
            strings.add(m.group(1));
        }
        return strings;
    }

    public ArrayList<String> getLink(String html) {
        ArrayList<String> strings = new ArrayList<>();
        Pattern link = Pattern.compile("href=\"(.*?)\"");
        Matcher m = link.matcher(html);
        while (m.find()) {
            strings.add(m.group(1));
        }
        return strings;
    }

    public String getNewsPageSplit(String html) {
        String result = null;
        Pattern narrow = Pattern.compile("<div class=\"lmf-col-content lmf-posts\">(.*?)<div class=\"lmf-col-sidebar\">");
        Matcher m = narrow.matcher(html);

        while (m.find()) {
            result = m.group(1);
        }
        return result;
    }

    public String getFullNewsPageSplit(String html) {
        String result = null;
        Pattern narrow = Pattern.compile("<div class=\"lmf-full-post\">(.*?)<div class=\"lmf-col-sidebar\">");
        Matcher m = narrow.matcher(html);

        while (m.find()) {
            result = m.group(1);
        }
        
        return result;
    }

    public ArrayList<String> analysePage(String html) {
        ArrayList<String> strings = new ArrayList<>();
        Pattern analyse = Pattern.compile("src=\"(.*?)\"|<p.*?>(.*?)</p>|<h2>(.*?)</h2>|<h1>(.*?)</h1>");
        Matcher m = analyse.matcher(html);

        while (m.find()) {
            strings.add(m.group(0));
        }
        return strings;
    }

    public String removeDefects(String text) {

        if (Build.VERSION.SDK_INT >= 24) {
            text = Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY).toString(); // for 24 api and more
        } else {
            text = Html.fromHtml(text).toString(); // or for older api
        }
        while(text.contains("\n")) {
            text = text.replace("\n", "");
        }
        
        return text;
    }

    public String narrowNearby(String html) {
        String result = null;
        Pattern narrow = Pattern.compile("<section class=\"lmf-sponser-cat-link\"><a href=\"//www.looemusic.co.uk/looe-music-festival-sponsors/things-to-do/\">THINGS TO DO</a></section>(.*?)<footer");
        Matcher m = narrow.matcher(html);

        while (m.find()) {
            result = m.group(1);
        }
        return result;
    }

    public String getFullNearbyPageSplit(String html) {
        String result = null;
        Pattern narrow = Pattern.compile("<div class=\"lmf-full-sponser\">(.*?)<div class=\"lmf_map\">");
        Matcher m = narrow.matcher(html);

        while (m.find()) {
            result = m.group(1);
        }
        return result;
    }
}

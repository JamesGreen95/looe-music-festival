package com.greenteam.LooeMusicFestival;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import static android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM;

public class ShuttleTimetable extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shuttle_timetable);

        getSupportActionBar().setDisplayOptions(DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.abs_layout);
        ((TextView) getSupportActionBar().getCustomView().findViewById(R.id.TITLE_TEXT)).setText(getString(R.string.shuttle_timetable));

    }

    public void back_button(View view) {
        finish();
    }
}

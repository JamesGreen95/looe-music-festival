package com.greenteam.LooeMusicFestival;

import android.app.Activity;
import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static io.fabric.sdk.android.Fabric.TAG;


public class LooeMusicFestivalApp extends Application implements DownloadPageMultiple.PageResponse, DownloadPage.PageResponse {

    ArrayList<Day> mDays = new ArrayList<>();


    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseAuth mAuth;
    private boolean mStagesLoaded = false;

    boolean loaded = false;

    StringSplits mStringSplits = new StringSplits();

    private String mEatHTML = "";
    private String mAccommodationHTML = "";
    private String mThingsToDoHTML = "";
    private String mNewsHTML = "";


    public void firebaseInitialise(Activity activity) {

        mAuth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };

        mAuth.signInAnonymously()
                .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInAnonymously:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInAnonymously", task.getException());
                        }
                    }
                });
        mAuth.addAuthStateListener(mAuthListener);
        retrieveSchedule();
    }

    public void retrieveSchedule() {

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference fDatabaseRoot = database.getReference("Days");

        fDatabaseRoot.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot stageSnapshot : dataSnapshot.getChildren()) {
                    Day day = new Day(stageSnapshot.getKey());
                    for (DataSnapshot stageNameSnapshot : stageSnapshot.getChildren()) {
                        String stageName = stageNameSnapshot.getKey();
                        Stage temp = new Stage(stageName);
                        for (DataSnapshot actsSnapshot : stageNameSnapshot.getChildren()) {
                            temp.addBand(actsSnapshot.getKey(), actsSnapshot.getValue().toString());
                        }
                        day.addStage(temp);
                    }
                    mDays.add(day);
                }

                mStagesLoaded = true;
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", databaseError.toException());
            }
        });
    }


    public ArrayList<Day> getmDays() {
        return mDays;
    }

    public void preloadPages() {
        if (getmNewsHTML().isEmpty()) {
            DownloadPage downloadPage = new DownloadPage(this);
            downloadPage.execute("https://www.looemusic.co.uk/news/");
        }

        if (getmAccommodationHTML().isEmpty() || mEatHTML.isEmpty() || mThingsToDoHTML.isEmpty()) {
            DownloadPageMultiple downloadPageMultiple = new DownloadPageMultiple(this);
            downloadPageMultiple.execute("https://www.looemusic.co.uk/looe-music-festival-sponsors/accommodation/", "https://www.looemusic.co.uk/looe-music-festival-sponsors/things-to-do/", "https://www.looemusic.co.uk/looe-music-festival-sponsors/eating-drinking/");
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        Date date = new Date();
        long dateOld = sharedPreferences.getLong("date", 0);
        long dateLong = date.getTime();

        if (dateOld + 86400000 >= dateLong) {
            mAccommodationHTML = sharedPreferences.getString("accommodation", "");
            mEatHTML = sharedPreferences.getString("eat", "");
            mThingsToDoHTML = sharedPreferences.getString("thingstodo", "");
            mNewsHTML = sharedPreferences.getString("news", "");
        }
    }

    public boolean ismStagesLoaded() {
        return mStagesLoaded;
    }

    @Override
    public void processFinish(ArrayList<String[]> output) {
        for (String[] array : output) {
            switch (array[0]) {
                case "https://www.looemusic.co.uk/looe-music-festival-sponsors/accommodation/":
                    mAccommodationHTML = mStringSplits.narrowNearby(array[1]);
                    break;
                case "https://www.looemusic.co.uk/looe-music-festival-sponsors/things-to-do/":
                    mThingsToDoHTML = mStringSplits.narrowNearby(array[1]);
                    break;
                case "https://www.looemusic.co.uk/looe-music-festival-sponsors/eating-drinking/":
                    mEatHTML = mStringSplits.narrowNearby(array[1]);
                    break;
            }
        }

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString("accommodation", mAccommodationHTML);
        edit.putString("eat", mEatHTML);
        edit.putString("thingstodo", mThingsToDoHTML);
        edit.apply();

    }

    public String getmEatHTML() {
        return mEatHTML;
    }

    public String getmAccommodationHTML() {
        return mAccommodationHTML;
    }

    public String getmThingsToDoHTML() {
        return mThingsToDoHTML;
    }

    public boolean isLoaded() {
        return loaded;
    }

    public void setLoaded(boolean loaded) {
        this.loaded = loaded;
    }

    @Override
    public void processFinish(String output) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        mNewsHTML = mStringSplits.getNewsPageSplit(output);
        edit.putString("news", mNewsHTML);
        Date date = new Date();
        edit.putLong("date", date.getTime());
        edit.apply();
    }

    public String getmNewsHTML() {
        return mNewsHTML;
    }
}

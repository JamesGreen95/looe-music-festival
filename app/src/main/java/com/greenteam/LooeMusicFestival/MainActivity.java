package com.greenteam.LooeMusicFestival;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "WYq4eBOH1zVJgbUTsg5od4SoG";
    private static final String TWITTER_SECRET = "	LXaUk8dTrqGcuNh9Gvhq577MnzQWNbu3q7o4T3OMwPdPuywsMM";




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        setContentView(R.layout.activity_main);

        LooeMusicFestivalApp looeMusicFestivalApp = (LooeMusicFestivalApp) getApplication();
        if(!looeMusicFestivalApp.isLoaded()) {
            looeMusicFestivalApp.firebaseInitialise(this);
            looeMusicFestivalApp.preloadPages();
            looeMusicFestivalApp.setLoaded(true);
        }

        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int mUIFlag;

        if(Build.VERSION.SDK_INT >= 19) {
            mUIFlag = View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_FULLSCREEN;
        } else {
            mUIFlag = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LOW_PROFILE;
        }
        decorView.setSystemUiVisibility(mUIFlag);
    }

    public void links(View view) {
        view.setClickable(false);
        Intent intent;
        String string = view.getTag().toString();

        switch (string) {
            case "schedule":
                intent = new Intent(MainActivity.this, Schedule.class);
                MainActivity.this.startActivity(intent);
                break;
            case "news":
                intent = new Intent(MainActivity.this, News.class);
                MainActivity.this.startActivity(intent);
                break;
            case "faqs":
                intent = new Intent(MainActivity.this, FAQs.class);
                MainActivity.this.startActivity(intent);
                break;
            case "shuttle_timetable":
                intent = new Intent(MainActivity.this, ShuttleTimetable.class);
                MainActivity.this.startActivity(intent);
                break;
            case "custom_schedule":
                intent = new Intent(MainActivity.this, CustomSchedule.class);
                MainActivity.this.startActivity(intent);
                break;
            case "social":
                intent = new Intent(MainActivity.this, Social.class);
                MainActivity.this.startActivity(intent);
                break;
            case "map":
                intent = new Intent(MainActivity.this, Map.class);
                MainActivity.this.startActivity(intent);
                break;
            case "nearby":
                intent = new Intent(MainActivity.this, Nearby.class);
                MainActivity.this.startActivity(intent);
                break;
        }
        view.setClickable(true);
    }
}
